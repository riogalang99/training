package com.ist.training.controller;


import com.ist.training.entitiy.CustomProduct;
import com.ist.training.entitiy.Matakuliah;
import com.ist.training.entitiy.Dosen;
import com.ist.training.entitiy.Mahasiswa;
import com.ist.training.repository.CustomProductDAO;
import com.ist.training.repository.DosenRepository;
import com.ist.training.repository.MatakuliahRepository;
import com.ist.training.repository.MahasiswaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class istController {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private MatakuliahRepository matakuliahRepository;

    @Autowired
    private CustomProductDAO dao;

    @GetMapping("/getAllMahasiswa")
    public ResponseEntity<List<Mahasiswa>> getAllUser() {
        List<Mahasiswa> result = mahasiswaRepository.findAll();
        if (result == null || result.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/getAllDosen")
    public ResponseEntity<List<Dosen>> getAllDosen() {
        List<Dosen> result = dosenRepository.findAll();
        if (result == null || result.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/getAllMatkul")
    public ResponseEntity<List<Matakuliah>> getAllMatakuliah() {
        List<Matakuliah> result = matakuliahRepository.findAll();
        if (result == null || result.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/saveMahasiswa")
    public ResponseEntity<Mahasiswa> saveMahasiswa(@RequestBody Mahasiswa user){
        try{
            Mahasiswa u = mahasiswaRepository.save(user);
            return new ResponseEntity<>(u, HttpStatus.CREATED);

        }catch(Exception e){
            log.error("Error gk mau nyimpan {}",e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/saveDosen")
    public ResponseEntity<Dosen> saveDosen(@RequestBody Dosen dosen){
        try{
            Dosen u = dosenRepository.save(dosen);
            return new ResponseEntity<>(u, HttpStatus.CREATED);

        }catch(Exception e){
            log.error("Error gk mau nyimpan {}",e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/saveMatkul")
    public ResponseEntity<Matakuliah> saveUser(@RequestBody Matakuliah matakuliah){
        try{
            Matakuliah u = matakuliahRepository.save(matakuliah);
            return new ResponseEntity<>(u, HttpStatus.CREATED);

        }catch(Exception e){
            log.error("Error gk mau nyimpan {}",e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/deleteUser")
    public ResponseEntity<String > deleteUser(@RequestParam String userId){

        try{
            mahasiswaRepository.deleteById(Long.valueOf(userId));
            return new ResponseEntity<>("Deleted", HttpStatus.OK);

        }catch(Exception e){
            log.error("Error delete user {}",e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getCustomProduct")
    public ResponseEntity<List<CustomProduct>> getCustomProduct() {
        List<CustomProduct> result = dao.getCustomProduct();
        if (result == null || result.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }





}
