package com.ist.training.entitiy;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@SqlResultSetMapping(name = "CustomProductQueryMap", entities = {
        @EntityResult(entityClass = CustomProduct.class,fields = {
                @FieldResult(name = "id", column = "id"),
                @FieldResult(name = "dosen", column = "Dosen"),
                @FieldResult(name = "mahasiswa", column = "Mahasiswa"),
                @FieldResult(name = "mengajar", column = "Mengajar")
        })
})

public class CustomProduct {
    @Id

    private Long id;
    private String dosen;
    private String mahasiswa;
    private String mengajar;
}
