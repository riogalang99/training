package com.ist.training.entitiy;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "matakuliah")

@Data
public class Matakuliah {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String nama_matkul;


    @OneToOne
    @JoinColumn(name = "matkul_id")
    @JsonBackReference
    private Dosen dosen;


}
