package com.ist.training.entitiy;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "mahasiswa")
@Data
public class Mahasiswa {
    @Id
    private Long id;
    private String username;

    @ManyToOne
    @JoinColumn(name = "kelas")
    @JsonBackReference

    private Dosen dosen;


}
