package com.ist.training.entitiy;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "dosen")
@Data
public class Dosen {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String nama;
    private String kelas_id;
    private String nama_matkul;

    @OneToMany(mappedBy = "dosen")
    private List<Mahasiswa> mahasiswa;



}
