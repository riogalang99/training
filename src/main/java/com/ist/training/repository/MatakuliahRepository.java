package com.ist.training.repository;

import com.ist.training.entitiy.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatakuliahRepository extends JpaRepository<Matakuliah, Long> {

}
