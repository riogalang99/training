package com.ist.training.repository;

import com.ist.training.entitiy.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DosenRepository extends JpaRepository<Dosen,Long> {
}
