package com.ist.training.repository;


import com.ist.training.entitiy.CustomProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomProductDAO {
    @Autowired
    private EntityManager en;

    public List<CustomProduct> getCustomProduct(){
       String notLivequery = "select RAND(100) as id, d.nama as Dosen, m.username as Mahasiswa , m2.nama_matkul as Mengajar \n" +
        "from dosen d \n" +
        "INNER JOIN mahasiswa m on d.kelas_id =m.kelas \n" +
        "INNER JOIN matakuliah m2 on d.nama_matkul = m2.matkul_id;";

        Query q = en.createNativeQuery(notLivequery,"CustomProductQueryMap");

        return q.getResultList();
    }
}
